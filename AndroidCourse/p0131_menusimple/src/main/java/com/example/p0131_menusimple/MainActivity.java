package com.example.p0131_menusimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public boolean onCreateOptionsMenu(Menu menu){
        // TODO Auto-generated method stub
        Log.d(TAG, "Вошли в метод onCreateOptionsMenu");
        menu.add("menu1");
        Log.d(TAG, "добавили меню1");
        menu.add("menu2");
        Log.d(TAG, "добавили меню2");
        menu.add("menu3");
        Log.d(TAG, "добавили меню3");
        menu.add("menu4");
        Log.d(TAG, "добавили меню4");

        Log.d(TAG, "вызываем super.onCreateOptionsMenu(menu);");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}
