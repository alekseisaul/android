package com.example.aleksejsaul.testfrominterview;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Text Views
    private TextView txtViewDate;
    private TextView txtViewTime;

    //CheckBox for 24-hour view activation
    private CheckBox chBx24HourView;

    //Data for DatePicker
    private int year_x;
    private int month_x;
    private int day_x;

    //Data for TimePicker
    private int hour_x;
    private int minute_x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initDateAndTime();
        initElements();

        //Set Date and Time in TextViews
        setDate();
        setTime();
    }

    private void initElements() {
        //Find Buttons
        Button btnShowDate = findViewById(R.id.btnShowDate);
        Button btnShowTime = findViewById(R.id.btnShowTime);

        //Find TextViews
        txtViewDate = findViewById(R.id.textDateView);
        txtViewTime = findViewById(R.id.textTimeView);

        //Find CheckBox
        chBx24HourView = findViewById(R.id.chBxEnable24Hour);
        chBx24HourView.setChecked(false);

        //Set Click Listeners for Buttons
        btnShowDate.setOnClickListener(this);
        btnShowTime.setOnClickListener(this);
        chBx24HourView.setOnClickListener(this);
    }

    private void initDateAndTime() {
        //Obtain current Date and Time
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);

        hour_x = cal.get(Calendar.HOUR_OF_DAY);
        minute_x = cal.get(Calendar.MINUTE);
    }

    private void setDate(){
        txtViewDate.setText(String.format(Locale.US,"%02d/%02d/%d", day_x, month_x + 1, year_x));
    }

    private void setTime() {
        if(chBx24HourView.isChecked()) {
            txtViewTime.setText(String.format(Locale.US,"%02d:%02d", hour_x, minute_x));
        } else {
            if(hour_x > 12){
                int hour = hour_x - 12;
                txtViewTime.setText(String.format(Locale.US,"%02d:%02d PM", hour, minute_x));
            } else {
                txtViewTime.setText(String.format(Locale.US,"%02d:%02d AM", hour_x, minute_x));
            }
        }
    }

    private final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month;
            day_x = dayOfMonth;
            setDate();
        }
    };

    private final TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour_x = hourOfDay;
            minute_x = minute;
            setTime();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShowDate:
                new DatePickerDialog(this, dateSetListener, year_x, month_x, day_x).show();
                break;
            case R.id.btnShowTime:
                new TimePickerDialog(this, timeSetListener, hour_x, minute_x, chBx24HourView.isChecked()).show();
            case R.id.chBxEnable24Hour:
                setTime();
            default:
                break;
        }
    }
}
